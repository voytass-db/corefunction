# CoreFunction Component

This component allows you to call any core PHP function in order to mock it's result inside your unit tests.

**Basic usage in class:**

```
## ./Example.php

use VT\Component\CoreFunction\CoreFunction;

class Example
{
    private $core;

    public function __construct(CoreFunction $core)
    {
        $this->core = $core;
    }
    
    public function foo(array $array): string
    {
        return $this->core->call('array_key_exists', 'key', $array) ? 'bar' : 'xyz';
    }
}
```

**Basic usage in test methods:**

- using standard PHPUnit mock objects

```
## ./tests/unit/ExampleTest.php

public function testFoo()
{
    $array = [
        'key' => 'value',
    ];

    $core = $this->createMock(CoreFunction::class);
    $core
        ->expects($this->once())
        ->method('call')
        ->with('array_key_exists', 'key', $array)
        ->willReturn(true)
    ;
    
    //=====
    
    $example = new Example($core);
    
    $this->assertSame('bar', $example->foo($array));
}
```

- using Prophecy

```
## ./tests/unit/ExampleTest.php

public function testFoo()
{
    $array = [
        'key' => 'value',
    ];

    $core = $this->prophesize(CoreFunction::class);
    $core
        ->call('array_key_exists', 'key', $array)
        ->shouldBeCalledTimes(1)
        ->willReturn(true)
    ;
    $core = $core->reveal();
    
    //=====
    
    $example = new Example($core);
    
    $this->assertSame('bar', $example->foo($array));
}
```

