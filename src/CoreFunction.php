<?php

declare (strict_types = 1);

namespace VT\Component\CoreFunction;

/**
 * Simple class that allows to call any PHP core function.
 *
 * @author Wojciech Brzeziński <wojciech.brzezinski@mail.com>
 */
class CoreFunction
{
    /**
     * Alias for "call()" method that allows to use instance of this class as a method.
     *
     * @param string $name
     * @param mixed  $arguments
     *
     * @return mixed
     */
    public function __invoke(string $name, ...$arguments)
    {
        return $this->call($name, ...$arguments);
    }

    /**
     * Allows to call core PHP function in order to mock it's result.
     *
     * @param string $name
     * @param mixed  $arguments
     *
     * @return mixed
     */
    public function call(string $name, ...$arguments)
    {
        return call_user_func_array('\\' . $name, $arguments);
    }
}
